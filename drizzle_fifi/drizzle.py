import math

import astropy.io.fits as pf
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import gc

from drizzle_fifi.write import drizzle_fits


def _plot_grids(xai_full, yai_full,
                xao, yao,
                pixsizei, pixsizeo,
                xcen, ycen,
                save=None, show=True, overlapping=0):
    xmini, ymini = xai_full.min(), yai_full.min()
    xmaxi, ymaxi = xai_full.max(), yai_full.max()
    if not (xao.min() - pixsizeo / 2. > xmini - pixsizei / 2. and
            yao.min() - pixsizeo / 2. > ymini - pixsizei / 2. and
            xao.max() + pixsizeo / 2. < xmaxi + pixsizei / 2. and
            yao.max() + pixsizeo / 2. < ymaxi + pixsizei / 2.):
        print('Weighting on the edges might work wrong!')
    plt.figure(figsize=(7, 7))
    'input pix centers (blue)'
    plt.plot(xai_full, yai_full, 'b.', label='Input grid')
    'adding input frame'
    fig = plt.gcf()
    ax = fig.gca()
    if overlapping == 0:
        rectangle = mpl.patches.Rectangle((xmini - pixsizei / 2., ymini - pixsizei / 2.), xmaxi - xmini + pixsizei,
                                          ymaxi - ymini + pixsizei, fill=False, edgecolor='b')
        ax.add_artist(rectangle)
    else:
        'input grid for testing overlapping'
        for i, j in zip(xai_full, yai_full):
            rectangle = mpl.patches.Rectangle((i - pixsizei / 2., j - pixsizei / 2.), pixsizei, pixsizei, fill=False,
                                              edgecolor='b')
            ax.add_artist(rectangle)
    'adding output grid'
    for i, j in zip(xao, yao):
        rectangle = mpl.patches.Rectangle((i - pixsizeo / 2., j - pixsizeo / 2.), pixsizeo, pixsizeo, fill=False,
                                          edgecolor='r')
        ax.add_artist(rectangle)
    'output pix centers'
    plt.plot(xao, yao, 'r.', label='Output grid')
    'center'
    plt.plot(xcen, ycen, 'go')
    # try:
    #     plt.plot(delta_x, delta_y, 'ko')
    # except NameError:
    #     pass
    'labels & x invert'
    plt.xlim(np.max(np.hstack((xai_full, xao))) + pixsizei, np.min(np.hstack((xai_full, xao))) - pixsizei)
    plt.ylim(np.min(np.hstack((yai_full, yao))) - pixsizei, np.max(np.hstack((yai_full, yao))) + pixsizei)
    plt.xlabel('$\\Delta$ RA [arcsec]')
    plt.ylabel('$\\Delta$ DEC [arcsec]')
    plt.legend()
    if save is not None:
        plt.savefig(save)
    if show:
        plt.show()
    plt.close()
    gc.collect()


def _drizzle_algorithm(npixx, npixy,
                       nfibers,
                       pixsizei, pixsizeo,
                       xai_full, yai_full,
                       xao, yao, xo, yo):
    a = np.zeros([nfibers, npixx * npixy])
    b = np.zeros([npixy, npixx])
    N = 100
    for i in range(npixx * npixy):
        'define input indices with any overlapping with fixed output pixel'
        ind = np.where(np.logical_and(np.abs(xao[i] - xai_full) < (pixsizei + pixsizeo) / 2.,
                                      np.abs(yao[i] - yai_full) < (pixsizei + pixsizeo) / 2.))[0]
        'initially all pixels are <-- --> & ^v'
        ax = np.ones(len(ind)) * pixsizeo
        ay = np.ones(len(ind)) * pixsizeo
        'ind1 for --> pixels'
        ind1 = np.where(xao[i] - pixsizeo / 2. < xai_full[ind] - pixsizei / 2.)[0]
        ax[ind1] = xao[i] - xai_full[ind][ind1] + (pixsizei + pixsizeo) / 2.
        'ind2 for <-- pixels'
        ind2 = np.where(xao[i] + pixsizeo / 2. > xai_full[ind] + pixsizei / 2.)[0]
        ax[ind2] = xai_full[ind][ind2] - xao[i] + (pixsizei + pixsizeo) / 2.
        'ind12 for --><-- pixels'
        ind12 = np.where(np.logical_and(xao[i] - pixsizeo / 2. < xai_full[ind] - pixsizei / 2.,
                                        xao[i] + pixsizeo / 2. > xai_full[ind] + pixsizei / 2.))[0]
        ax[ind12] = pixsizei
        'ind3 for ^ pixels'
        ind3 = np.where(yao[i] - pixsizeo / 2. < yai_full[ind] - pixsizei / 2.)[0]
        ay[ind3] = yao[i] - yai_full[ind][ind3] + (pixsizei + pixsizeo) / 2.
        'ind4 for v pixels'
        ind4 = np.where(yao[i] + pixsizeo / 2. > yai_full[ind] + pixsizei / 2.)[0]
        ay[ind4] = yai_full[ind][ind4] - yao[i] + (pixsizei + pixsizeo) / 2.
        'ind34 for ^v pixels'
        ind34 = np.where(np.logical_and(yao[i] - pixsizeo / 2. < yai_full[ind] - pixsizei / 2.,
                                        yao[i] + pixsizeo / 2. > yai_full[ind] + pixsizei / 2.))[0]
        ay[ind34] = pixsizei
        a[ind, i] = ax * ay
        'edges weighting'
        b_fixed_pixel = np.ones([N, N])
        ind13 = np.where(np.all([xao[i] - pixsizeo / 2. < xai_full[ind] - pixsizei / 2.,
                                 yao[i] - pixsizeo / 2. < yai_full[ind] - pixsizei / 2.], axis=0))[0]
        ind23 = np.where(np.all([xao[i] + pixsizeo / 2. > xai_full[ind] + pixsizei / 2.,
                                 yao[i] - pixsizeo / 2. < yai_full[ind] - pixsizei / 2.], axis=0))[0]
        ind14 = np.where(np.all([xao[i] - pixsizeo / 2. < xai_full[ind] - pixsizei / 2.,
                                 yao[i] + pixsizeo / 2. > yai_full[ind] + pixsizei / 2.], axis=0))[0]
        ind24 = np.where(np.all([xao[i] + pixsizeo / 2. > xai_full[ind] + pixsizei / 2.,
                                 yao[i] + pixsizeo / 2. > yai_full[ind] + pixsizei / 2.], axis=0))[0]
        for j in ind13:
            b_fixed_pixel[-int(ay[j] * N / pixsizeo):, -int(ax[j] * N / pixsizeo):] = 0.
        for j in ind23:
            b_fixed_pixel[0:int(ay[j] * N / pixsizeo), -int(ax[j] * N / pixsizeo):] = 0.
        for j in ind14:
            b_fixed_pixel[-int(ay[j] * N / pixsizeo):, 0:int(ax[j] * N / pixsizeo)] = 0.
        for j in ind24:
            b_fixed_pixel[0:int(ay[j] * N / pixsizeo), 0:int(ax[j] * N / pixsizeo)] = 0.
        b[yo[i], xo[i]] = b_fixed_pixel.sum() * pixsizeo ** 2. / N ** 2.
    return a, b


def drizzle(arguments, outputdir):
    inputfile = outputdir + 'rss.fits'

    pixsizei = float(arguments.input_pixsize)  # [arcsec]
    pixsizeo = float(arguments.output_pixsize)  # [arcsec]

    RSS_HDU = pf.open(inputfile)
    RSS_HDU.info()
    RSS = RSS_HDU['FLUX_WITHOUT_NANS'].data
    eRSS = RSS_HDU['ERROR_WITHOUT_NANS'].data
    xai_full = RSS_HDU['X'].data
    yai_full = RSS_HDU['Y'].data
    wl = RSS_HDU['WL'].data
    RSS[np.isnan(RSS)] = 0.
    nfibers = RSS.shape[0]
    nwl = RSS.shape[1]
    # try:
    #     ra = arguments.ra
    #     dec = arguments.dec
    #     delta_x = (ra - RSS_HDU[0].header['OBSLAM']) * 3600.
    #     delta_y = (dec - RSS_HDU[0].header['OBSBET']) * 3600.
    # except TypeError:
    #     print('input the coordinates to check the center')
    #     pass

    xmini = xai_full.min()
    xmaxi = xai_full.max()
    ymini = yai_full.min()
    ymaxi = yai_full.max()
    xmaxo = xmaxi
    ymaxo = ymaxi

    'the output center and number of pixels can either be specified in the logfile, or left default'
    'the default parameters will appear in the logfile and can be adjusted'
    try:
        xcen = float(arguments.xcen)  # in [arcsec]
    except AttributeError:
        xcen = 0.
        with open(arguments.logfile, 'a') as logfile:
            logfile.write(f'xcen {xcen}\n')
    try:
        ycen = float(arguments.ycen)  # in [arcsec]
    except AttributeError:
        ycen = 0.
        with open(arguments.logfile, 'a') as logfile:
            logfile.write(f'ycen {ycen}\n')
    try:
        xcen_pixel = float(arguments.xcen_pixel)  # in [arcsec]
        xmino = xcen - xcen_pixel * pixsizeo
    except AttributeError:
        xmino = xcen - math.floor((xcen - xmini) / pixsizeo) * pixsizeo - (pixsizei - pixsizeo)
        xcen_pixel = (xcen - xmino) / pixsizeo
        with open(arguments.logfile, 'a') as logfile:
            logfile.write(f'xcen_pixel {xcen_pixel}\n')
    try:
        ycen_pixel = float(arguments.ycen_pixel)  # in [arcsec]
        ymino = ycen - ycen_pixel * pixsizeo
    except AttributeError:
        ymino = ycen - math.floor((ycen - ymini) / pixsizeo) * pixsizeo - (pixsizei - pixsizeo)
        ycen_pixel = (ycen - ymino) / pixsizeo
        with open(arguments.logfile, 'a') as logfile:
            logfile.write(f'ycen_pixel {ycen_pixel}\n')
    try:
        npixx = int(arguments.npixx)
    except AttributeError:
        npixx = int(math.floor((xmaxo - xmino) / pixsizeo + pixsizei / pixsizeo))
        with open(arguments.logfile, 'a') as logfile:
            logfile.write(f'npixx {npixx}\n')
    try:
        npixy = int(arguments.npixy)
    except AttributeError:
        npixy = int(math.floor((ymaxo - ymino) / pixsizeo + pixsizei / pixsizeo))
        with open(arguments.logfile, 'a') as logfile:
            logfile.write(f'npixy {npixy}\n')
    print('The number of output pixels: {0} by {1}'.format(npixx, npixy))

    indiceso = np.indices((npixy, npixx))
    xo = indiceso[1].flatten()
    yo = indiceso[0].flatten()
    xao = xo * pixsizeo + xmino
    yao = yo * pixsizeo + ymino
    xmaxo, ymaxo = xao.max(), yao.max()

    _plot_grids(xai_full, yai_full,
                xao, yao,
                pixsizei, pixsizeo,
                xcen, ycen,
                save=outputdir + 'grid.png',
                show=arguments.plot)

    Ioutput = np.zeros([nwl, npixy, npixx])
    Eoutput = np.zeros([nwl, npixy, npixx])
    a, b = _drizzle_algorithm(npixx, npixy,
                              nfibers,
                              pixsizei, pixsizeo,
                              xai_full, yai_full,
                              xao, yao, xo, yo)
    for w in range(nwl):
        Ioutput[w, yo[:], xo[:]] = np.dot(RSS[:, w], a)
        Eoutput[w, yo[:], xo[:]] = np.dot(eRSS[:, w] ** 2., a ** 2.)
        'normalization'
        Ioutput[w, yo[:], xo[:]] = Ioutput[w, yo[:], xo[:]] / (a.sum(axis=0) + b[yo[:], xo[:]])
        Eoutput[w, yo[:], xo[:]] = np.sqrt(Eoutput[w, yo[:], xo[:]]) / (a.sum(axis=0) + b[yo[:], xo[:]])

    'flux depends on pixel size'
    Ioutput = Ioutput * pixsizeo ** 2. / pixsizei ** 2.
    Eoutput = Eoutput * pixsizeo ** 2. / pixsizei ** 2.

    drizzle_fits(RSS_HDU[0].header,
                 Ioutput, Eoutput,
                 xcen_pixel, ycen_pixel,
                 pixsizeo,
                 xcen, ycen,
                 outputdir + 'drizzle.fits')
