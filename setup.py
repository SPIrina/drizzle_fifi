from setuptools import setup

setup(
    name='drizzle_fifi',
    version='1.0',
    packages=['drizzle_fifi'],
    scripts=['bin/drizzle_fifi'],
    # url='https://gitlab.com/SPIrina/drizzle_fifi',
    license='MIT',
    author='Irina Smirnova-Pinchukova',
    author_email='irinasmirnovapinchukova@gmail.com',
    description='Drizzle_fifi is a modified reduction step for FIFI-LS instrument of SOFIA observatory.',
    install_requires=[
        'numpy>=1.15.4,<1.16',
        'astropy>=3.1',
        'matplotlib>=3.0.2',
        'emcee>=2.2.1'
    ]
)
