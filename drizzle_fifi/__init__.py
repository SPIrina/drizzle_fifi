"""
drizzle_fifi - a package to apply a modified reduction to FIFI-LS/SOFIA data.

© Irina Smirnova-Pinchukova https://spirina.gitlab.io/
"""
from drizzle_fifi.run import run
from drizzle_fifi.rss import rss
from drizzle_fifi.drizzle import drizzle
from drizzle_fifi import write
