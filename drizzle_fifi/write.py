import astropy.io.fits as pf


def rss_fits(header,
             uncorrected_flux, uncorrected_error,
             new_flux, new_error,
             x_coordinates, y_coordinates,
             wl,
             outputfile):
    with pf.HDUList([pf.PrimaryHDU(uncorrected_flux),
                     pf.ImageHDU(uncorrected_error, name='ERROR'),
                     pf.ImageHDU(new_flux, name='FLUX_WITHOUT_NANS'),
                     pf.ImageHDU(new_error, name='ERROR_WITHOUT_NANS'),
                     pf.ImageHDU(x_coordinates, name='X'),
                     pf.ImageHDU(y_coordinates, name='Y'),
                     pf.ImageHDU(wl, name='WL')]) as out:
        out[0].header['OBJECT'] = header['OBJECT']
        out[0].header['BUNIT'] = header['BUNIT']
        out[0].header['CRVAL3'] = header['CRVAL3']
        out[0].header['CRPIX3'] = header['CRPIX3']
        out[0].header['CTYPE3'] = header['CTYPE3']
        out[0].header['CDELT3'] = header['CDELT3']
        out[0].header['COORDSYS'] = header['COORDSYS']
        out[0].header['OBSLAM'] = header['OBSLAM']  # [deg] observed coordinates
        out[0].header['OBSBET'] = header['OBSBET']  # [deg]
        out[0].header['OBSRA'] = header['OBSRA'] * 15.  # [deg] requested coordinates
        out[0].header['OBSDEC'] = header['OBSDEC']  # [deg]
        out.writeto(outputfile, overwrite=True)


def drizzle_fits(rss_header,
                 Ioutput, Eoutput,
                 xcen_pixel, ycen_pixel,
                 pixsizeo,
                 xcen, ycen,
                 outputfile):
    with pf.HDUList([pf.PrimaryHDU(Ioutput),
                     pf.ImageHDU(Eoutput, name='ERROR'),
                     pf.ImageHDU(Ioutput/Eoutput, name='SNR'),
                     ]) as out:
        out[0].header['CRPIX1'] = xcen_pixel + 1.  # fits starts with 1
        out[0].header['CRPIX2'] = ycen_pixel + 1.
        out[0].header['CRPIX3'] = rss_header['CRPIX3']
        # out[0].header['CRVAL1'] = header['OBSLAM']  # [deg]
        # out[0].header['CRVAL2'] = header['OBSBET']  # [deg]
        out[0].header['OBSLAM'] = rss_header['OBSLAM']  # [deg]
        out[0].header['OBSBET'] = rss_header['OBSBET']  # [deg]
        # out[0].header['OBJLAM'] = ra  # [deg]
        # out[0].header['OBJBET'] = dec  # [deg]
        out[0].header['CRVAL1'] = xcen
        out[0].header['CRVAL2'] = ycen
        out[0].header['CRVAL3'] = rss_header['CRVAL3']
        # out[0].header['CTYPE1'] = str('RA---TAN')
        # out[0].header['CTYPE2'] = str('DEC--TAN')
        out[0].header['CTYPE3'] = rss_header['CTYPE3']
        out[0].header['CDELT1'] = pixsizeo  # [arcsec]
        out[0].header['CDELT2'] = pixsizeo  # [arcsec]
        out[0].header['CDELT3'] = rss_header['CDELT3']
        out[0].header['CUNIT1'] = str('arcsec')
        out[0].header['CUNIT2'] = str('arcsec')
        out[0].header['CUNIT3'] = str('um')
        out.writeto(outputfile, overwrite=True)
