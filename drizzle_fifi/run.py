import time
import os
import drizzle_fifi.rss
import drizzle_fifi.drizzle


def run(arguments):
    start_time = time.time()

    'define datadir & outputdir'
    try:
        datadir = arguments.datadir
        print('Data directory: {}'.format(datadir))
    except AttributeError:
        raise AttributeError('datadir argument is mandatory')
    try:
        outputdir = arguments.outputdir
    except AttributeError:
        outputdir = datadir
    try:
        os.makedirs(outputdir)
        print('Output directory ', outputdir, 'created')
    except FileExistsError:
        print('Output directory: {}'.format(outputdir))

    rss_time = time.time()
    drizzle_fifi.rss(arguments, outputdir)
    print('RSS done {:.4f} min'.format((time.time() - rss_time) / 60.))

    drizzle_time = time.time()
    drizzle_fifi.drizzle(arguments, outputdir)
    print('Drizzle done {:.4f} min'.format((time.time() - drizzle_time) / 60.))

    print('Finished {:.2f} min'.format((time.time() - start_time) / 60.))
