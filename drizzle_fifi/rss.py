import astropy.io.fits as pf
import numpy as np
import matplotlib.pyplot as plt
import gc

from drizzle_fifi.write import rss_fits


def _weighted_std(values, weights=None, **kwargs):
    average = np.average(values, weights=weights, **kwargs)
    variance = np.average((values - average) ** 2, weights=weights, **kwargs)
    return np.sqrt(variance)


def _bkg_subtraction(flux, error, wl_ind_background, bkg=1):
    pixel_number = flux.shape[0]
    mean = np.zeros([pixel_number])
    sigma = np.zeros([pixel_number])
    for i in range(pixel_number):  # for each pixel; -1 is due to the clipped one
        not_nan_ind = ~np.isnan(flux[i, wl_ind_background])
        if not_nan_ind.sum() > 0:
            mean[i] = np.average(flux[i, wl_ind_background][not_nan_ind],
                                 weights=1. / error[i, wl_ind_background][not_nan_ind] ** 2.)
            sigma[i] = _weighted_std(flux[i, wl_ind_background][not_nan_ind],
                                     weights=1. / error[i, wl_ind_background][not_nan_ind] ** 2.)
            if np.isnan(mean[i]) or np.isnan(sigma[i]):
                print('Warning! Wrong mean & sigma! i = {0}; FIR = {1}; FIR error = {2}'
                      .format(i, mean[i], sigma[i]))
            ind = np.isnan(flux[i, :])
            if bkg == 1:
                flux[i, :] = flux[i, :] - mean[i]
                flux[i, :][ind] = np.random.normal(loc=0., scale=sigma[i], size=ind.sum())
            else:
                flux[i, :][ind] = np.random.normal(loc=mean[i], scale=sigma[i], size=ind.sum())
            error[i, :][ind] = sigma[i]
        else:
            print('This pixel does not contain any data:', i, name)
            flux[i, :] = flux[i, :]
            error[i, :] = error[i, :]
            mean[i] = np.nan
            sigma[i] = np.nan
    return flux, error, mean, sigma


def _add2rss(hdu, line, line_width,
             uncorrected_flux, uncorrected_error,
             new_flux, new_error,
             x_coordinates, y_coordinates,
             n_input,
             hot_pixel=False,
             subtract_bkg=1):
    if hot_pixel:
        spatial_index = -1
    else:
        spatial_index = None
    flux = hdu['UNCORRECTED_FLUX'].data[:spatial_index, :]  # [spatial, spectral]
    error = hdu['UNCORRECTED_ERROR'].data[:spatial_index, :]
    x = hdu['X'].data[:spatial_index]
    y = hdu['Y'].data[:spatial_index]
    wl = hdu['WAVELENGTH'].data
    wl_ind_background = np.where(abs(wl - line) >= line_width)[0]
    'plot atran'
    atran = hdu['TRANSMISSION'].data
    plt.plot(wl, atran, '-')

    if n_input == 0:
        uncorrected_flux = flux + 0.
        uncorrected_error = error + 0.
        flux, error, mean, sigma = _bkg_subtraction(flux, error, wl_ind_background)
        new_flux = flux
        new_error = error
        x_coordinates = x
        y_coordinates = y
    else:
        uncorrected_flux = np.append(uncorrected_flux, flux, axis=0)
        uncorrected_error = np.append(uncorrected_error, error, axis=0)
        flux, error, mean, sigma = _bkg_subtraction(flux, error, wl_ind_background, bkg=subtract_bkg)
        new_flux = np.append(new_flux, flux, axis=0)
        new_error = np.append(new_error, error, axis=0)
        x_coordinates = np.append(x_coordinates, x, axis=0)
        y_coordinates = np.append(y_coordinates, y, axis=0)
    return (uncorrected_flux, uncorrected_error,
            new_flux, new_error,
            x_coordinates, y_coordinates)


def rss(arguments, outputdir):
    np.random.seed(6)

    'read the files'
    datadir = arguments.datadir
    n_input = 0
    altitude = []
    baroaltitude = []
    elevation = []
    line = float(arguments.line)
    line_width = float(arguments.line_width)
    try:
        hot_pixel = bool(int(arguments.hot_pixel))
    except AttributeError:
        hot_pixel = False
    try:
        subtract_bkg = int(arguments.subtract_bkg)
    except AttributeError:
        subtract_bkg = 1

    uncorrected_flux = np.empty([0])
    uncorrected_error = np.empty([0])
    new_flux = np.empty([0])
    new_error = np.empty([0])
    x_coordinates = np.empty([0])
    y_coordinates = np.empty([0])
    with open(datadir + arguments.files, 'r') as files:
        for name in files.read().splitlines():
            hdu = pf.open(datadir + name, ignore_missing_end=True)
            altitude = altitude + [hdu[0].header['ALTITUDE']]
            baroaltitude = baroaltitude + [hdu[0].header['HIERARCH BAROALTITUDE']]
            elevation = elevation + [hdu[0].header['TELEL']]
            (uncorrected_flux, uncorrected_error,
             new_flux, new_error,
             x_coordinates, y_coordinates) = _add2rss(hdu, line, line_width,
                                                      uncorrected_flux, uncorrected_error,
                                                      new_flux, new_error,
                                                      x_coordinates, y_coordinates,
                                                      n_input,
                                                      hot_pixel=hot_pixel,
                                                      subtract_bkg=subtract_bkg)
            n_input += 1
    print(f'The number of files = {n_input}')
    if arguments.plot:
        plt.show()
    plt.close()

    'plot header keys - altitude and elevation angle'
    fig1 = plt.figure()
    plt.plot(altitude, 'o-', label='GPS')
    plt.plot(baroaltitude, 'o-', label='Barometric')
    plt.ylabel('Altitude [feet]')
    plt.xlabel('Files #')
    plt.legend()
    plt.savefig(outputdir + 'altitude.png')
    fig2 = plt.figure()
    plt.plot(elevation, 'o-')
    plt.ylabel('Elevation [deg]')
    plt.xlabel('Files #')
    plt.savefig(outputdir + 'elevation.png')
    if arguments.plot:
        plt.show()
    plt.close('all')
    np.savetxt(outputdir + 'altitude.txt', baroaltitude)
    np.savetxt(outputdir + 'elevation.txt', elevation)

    rss_fits(hdu[0].header,
             uncorrected_flux, uncorrected_error,
             new_flux, new_error,
             x_coordinates, y_coordinates,
             hdu['WAVELENGTH'].data,
             outputdir + 'rss.fits')

    print(f'RSS file created: {outputdir}rss.fits')

    gc.collect()
