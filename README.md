# Drizzle FIFI-LS


# Installation
```
python setup.py develop
```
# Example
Go to example folder and find there LEVEL 3 'WGR' 
files for the RED channel (contains the [CII] line). 
Create a file with the sorted list of thise names. 
You could run this command in the terminal for 
this:
```
ls *WGR* > files
```

Check logfile.txt. All the information is stored 
there in the format 

_argument value_of_the_argument_

Each line should contain only one spacebar, 
separating the argument from its value.

Change _datadir_ and _outputdir_ values according to 
your filesystem.

Run the example with the following command 
if you're in the **example** folder 
(provide the full path to the logfile.txt file 
to run from any folder)
```
drizzle_fifi -l ./logfile.txt -p
```
The additional `-p` will show the plots, but the 
plots will be saved in the _outputdir_ anyway.

If everything goes how it intended there will be 
_rss.fits_ and _drizzle.fits_ in the output directory 
as well as the bunch of plots and _.txt_ files. 

#### Logfile arguments
The important arguments:
```
datadir <data directory path; mandatory argument>
outputdir <output directory path; default = datadir>
files files  # this is the sorted list of files you want to use
line 163.35  # microns
line_width 0.2  # microns
subtract_bkg 1  # additional  background subtraction
hot_pixel 1  # 1 to exclude the well-known hot pixel; 0 to keep it
input_pixsize 12.  # 12 for the RED channel; 6. for BLUE
output_pixsize 6.  # the desired output pixel size in arcsec
```
When you run the example, you can find another set of arguments 
is added to the end of the file. Try changing it to some other 
values and see how the grid is changing. Try this:
```
xcen 3.
ycen 0.
xcen_pixel 5
ycen_pixel 5
npixx 11
npixy 11
```
The description of those for x-axis:
```
xcen <central output pixel is at this coordinate in arcsec>
xcen_pixel <xcen corresponds to this pixel>
npixx <number of output pixels>
```

Check _write.py_ `drizzle_fits` function to check how these 
arguments relate to the standard fits header.